var kalgorithm_8h =
[
    [ "MappedInterval", "classfrechet_1_1k_1_1MappedInterval.html", "classfrechet_1_1k_1_1MappedInterval" ],
    [ "kAlgorithm", "classfrechet_1_1k_1_1kAlgorithm.html", "classfrechet_1_1k_1_1kAlgorithm" ],
    [ "WorkingSet", "structfrechet_1_1k_1_1kAlgorithm_1_1WorkingSet.html", "structfrechet_1_1k_1_1kAlgorithm_1_1WorkingSet" ],
    [ "Greedy", "structfrechet_1_1k_1_1kAlgorithm_1_1Greedy.html", "structfrechet_1_1k_1_1kAlgorithm_1_1Greedy" ],
    [ "BruteForce", "structfrechet_1_1k_1_1kAlgorithm_1_1BruteForce.html", "structfrechet_1_1k_1_1kAlgorithm_1_1BruteForce" ],
    [ "KWorkerJob", "classfrechet_1_1k_1_1KWorkerJob.html", "classfrechet_1_1k_1_1KWorkerJob" ],
    [ "IntervalMap", "kalgorithm_8h.html#aca1dcb9fbe3f0423159e13cf3ac77d6e", null ]
];