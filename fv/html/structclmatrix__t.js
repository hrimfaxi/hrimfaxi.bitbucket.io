var structclmatrix__t =
[
    [ "data", "structclmatrix__t.html#abf0316344d588cc427eca0d3082428d7", null ],
    [ "local_data", "structclmatrix__t.html#a7eaa62ba13c4500b35299ba0f5521431", null ],
    [ "ncols", "structclmatrix__t.html#a5962ee33dcc7ebe8583b235483e907ee", null ],
    [ "nrows", "structclmatrix__t.html#a76053a15f37edf7283edf2a9d3a440ff", null ],
    [ "padded_cols", "structclmatrix__t.html#aefb597d47c8bcdd7f1a430b4be9415f1", null ],
    [ "padded_rows", "structclmatrix__t.html#a49aa4cda91ea180262cf664ff266bc54", null ],
    [ "width", "structclmatrix__t.html#abaf8d14a73b1c86b25fcc5f168b9c2ea", null ]
];