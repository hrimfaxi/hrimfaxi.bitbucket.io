var dir_9662bae60b11d96d68acf831a4ba0644 =
[
    [ "algorithm.h", "algorithm_8h.html", "algorithm_8h" ],
    [ "double_queue.h", "double__queue_8h.html", [
      [ "DoubleEndedQueue", "classfrechet_1_1poly_1_1DoubleEndedQueue.html", "classfrechet_1_1poly_1_1DoubleEndedQueue" ],
      [ "Frame", "structfrechet_1_1poly_1_1DoubleEndedQueue_1_1Frame.html", "structfrechet_1_1poly_1_1DoubleEndedQueue_1_1Frame" ]
    ] ],
    [ "double_queue_impl.h", "double__queue__impl_8h.html", null ],
    [ "jobs.h", "jobs_8h.html", [
      [ "PolygonWorkerJob", "classfrechet_1_1poly_1_1PolygonWorkerJob.html", "classfrechet_1_1poly_1_1PolygonWorkerJob" ],
      [ "DecideWorkerJob", "classfrechet_1_1poly_1_1DecideWorkerJob.html", "classfrechet_1_1poly_1_1DecideWorkerJob" ],
      [ "OptimisePolyWorkerJob", "classfrechet_1_1poly_1_1OptimisePolyWorkerJob.html", "classfrechet_1_1poly_1_1OptimisePolyWorkerJob" ],
      [ "OptimiseCurveWorkerJob", "classfrechet_1_1poly_1_1OptimiseCurveWorkerJob.html", "classfrechet_1_1poly_1_1OptimiseCurveWorkerJob" ]
    ] ],
    [ "optimise_impl.h", "optimise__impl_8h.html", null ],
    [ "parallel.h", "parallel_8h.html", [
      [ "AlgorithmSingleCore", "classfrechet_1_1poly_1_1AlgorithmSingleCore.html", "classfrechet_1_1poly_1_1AlgorithmSingleCore" ],
      [ "AlgorithmMultiCore", "classfrechet_1_1poly_1_1AlgorithmMultiCore.html", "classfrechet_1_1poly_1_1AlgorithmMultiCore" ],
      [ "AlgorithmTopoSort", "classfrechet_1_1poly_1_1AlgorithmTopoSort.html", "classfrechet_1_1poly_1_1AlgorithmTopoSort" ]
    ] ],
    [ "poly_path.h", "poly__path_8h.html", [
      [ "PolygonFSPath", "classfrechet_1_1poly_1_1PolygonFSPath.html", "classfrechet_1_1poly_1_1PolygonFSPath" ]
    ] ],
    [ "poly_utils.h", "poly__utils_8h.html", "poly__utils_8h" ],
    [ "shortest_paths.h", "shortest__paths_8h.html", [
      [ "PolygonShortestPaths", "classfrechet_1_1poly_1_1PolygonShortestPaths.html", "classfrechet_1_1poly_1_1PolygonShortestPaths" ],
      [ "PolygonShortestPathsFS", "classfrechet_1_1poly_1_1PolygonShortestPathsFS.html", "classfrechet_1_1poly_1_1PolygonShortestPathsFS" ]
    ] ],
    [ "triangulation.h", "triangulation_8h.html", "triangulation_8h" ],
    [ "types.h", "frechet_2poly_2types_8h.html", "frechet_2poly_2types_8h" ]
];