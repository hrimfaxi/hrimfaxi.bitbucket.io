var searchData=
[
  ['q',['Q',['../classfrechet_1_1view_1_1GraphicsHoverLineItem.html#ad46ea1f6935e615c037e99c196fd8fc8abd27c8e86fb686443f1a66fbfc50419e',1,'frechet::view::GraphicsHoverLineItem']]],
  ['q_5fconvex',['Q_CONVEX',['../classfrechet_1_1poly_1_1Algorithm.html#aeb86425351d5a8c7df7d5005d604b7a2a7b5665a18ddaff32478ccfda0bed2d2b',1,'frechet::poly::Algorithm']]],
  ['q_5fempty',['Q_EMPTY',['../classfrechet_1_1poly_1_1Algorithm.html#aeb86425351d5a8c7df7d5005d604b7a2aa5d4e29973a1c44ba9dccccbe92bb119',1,'frechet::poly::Algorithm']]],
  ['q_5fnot_5fclosed',['Q_NOT_CLOSED',['../classfrechet_1_1poly_1_1Algorithm.html#aeb86425351d5a8c7df7d5005d604b7a2ade9ec64929773bb8ef38eca6c8d1826a',1,'frechet::poly::Algorithm']]],
  ['q_5fnot_5fcounter_5fclockwise',['Q_NOT_COUNTER_CLOCKWISE',['../classfrechet_1_1poly_1_1Algorithm.html#aeb86425351d5a8c7df7d5005d604b7a2a3ffb4423f541660cf83a7e042acf3d82',1,'frechet::poly::Algorithm']]],
  ['q_5fnot_5fsimple',['Q_NOT_SIMPLE',['../classfrechet_1_1poly_1_1Algorithm.html#aeb86425351d5a8c7df7d5005d604b7a2a9f9095965d26d046c978587bac33537b',1,'frechet::poly::Algorithm']]],
  ['qdiag',['Qdiag',['../classfrechet_1_1view_1_1GraphicsHoverLineItem.html#ad46ea1f6935e615c037e99c196fd8fc8aaed852e96c1b407e778fd996700e95c0',1,'frechet::view::GraphicsHoverLineItem']]]
];
