var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefgiklmoprstvwx",
  2: "cfu",
  3: "abcdfgijklmnopstw",
  4: "abcdefghijklmnopqrstuvwz~",
  5: "_abcdefghijklmnopqrstuvwxz",
  6: "abcdefgiklmnopstv",
  7: "abdlorstw",
  8: "abcdefhlmnopqrstuvy",
  9: "e",
  10: "bcdfgkmsw",
  11: "_abcdfimpqrstuw",
  12: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties",
  10: "related",
  11: "defines",
  12: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Properties",
  10: "Friends",
  11: "Macros",
  12: "Pages"
};

