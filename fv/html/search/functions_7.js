var searchData=
[
  ['h',['h',['../classPath.html#ad0cc2cd6b972618c450dc7728b3befc7',1,'Path::h(double h)'],['../classPath.html#af01325f1944ec173917072d195fba9dd',1,'Path::H(double h)']]],
  ['halfplanetest',['halfplaneTest',['../classfrechet_1_1poly_1_1PolygonShortestPaths.html#a946fcbb06f43fc75946496ffd3010521',1,'frechet::poly::PolygonShortestPaths']]],
  ['handlemessage',['handleMessage',['../classfrechet_1_1input_1_1InputReader_1_1XmlMessageHandler.html#a83bb5ef3aae468ff07a8a3f8d8a4c5d8',1,'frechet::input::InputReader::XmlMessageHandler']]],
  ['has_5fbits',['has_bits',['../classfrechet_1_1reach_1_1Graph.html#a78b3eb45ec7f47d0db7b46c16ff6057a',1,'frechet::reach::Graph']]],
  ['hasgpusupport',['hasGpuSupport',['../classfrechet_1_1app_1_1ConcurrencyContext.html#a63773af7c23f48b6b1b1e4d179593a04',1,'frechet::app::ConcurrencyContext']]],
  ['height',['height',['../structfrechet_1_1data_1_1Rect.html#a1b1abd8a3e0178332a38a58b12c3bfdf',1,'frechet::data::Rect']]],
  ['hideresult',['hideResult',['../classfrechet_1_1view_1_1ControlPanel.html#a31f6e27ba83f13d1a6a7b73c6e236391',1,'frechet::view::ControlPanel::hideResult()'],['../classfrechet_1_1view_1_1FreeSpaceView.html#aa80fe075ee25f76da2b80ab474414be9',1,'frechet::view::FreeSpaceView::hideResult()']]],
  ['hilitepoint',['hilitePoint',['../classfrechet_1_1view_1_1FreeSpaceView.html#a7d5d0168e83c56d5b3519d1c9bc8e1c4',1,'frechet::view::FreeSpaceView']]],
  ['hilitesegment',['hiliteSegment',['../classfrechet_1_1view_1_1CurveView.html#a89a7ad2c8d8b02b7a9b3c489df65e6ab',1,'frechet::view::CurveView::hiliteSegment()'],['../classfrechet_1_1view_1_1FreeSpaceView.html#aa7a8d45cf680df39484ed8ac6d45b3c1',1,'frechet::view::FreeSpaceView::hiliteSegment()']]],
  ['hmask',['hmask',['../classfrechet_1_1reach_1_1Graph.html#a1975d4c2a514351f19d9e6a73b26e911',1,'frechet::reach::Graph']]],
  ['hor',['hor',['../classfrechet_1_1fs_1_1Grid.html#ae8a8a591cedc9dac2770a0c826b59d72',1,'frechet::fs::Grid::hor() const'],['../classfrechet_1_1fs_1_1Grid.html#a61cafe4619069e5e3e02e151149a4e51',1,'frechet::fs::Grid::hor()']]],
  ['horiz',['horiz',['../classfrechet_1_1reach_1_1GraphModel.html#a5da7a4451ffa874008eaf55f7f58b973',1,'frechet::reach::GraphModel::horiz()'],['../classfrechet_1_1reach_1_1GraphModel.html#ae3f58732a1bcd37c8fdcc19528682d52',1,'frechet::reach::GraphModel::horiz() const']]],
  ['horizontal',['horizontal',['../classfrechet_1_1reach_1_1Structure.html#a53560d416a7a56a0339855ebd79174db',1,'frechet::reach::Structure']]],
  ['horizontalgridline',['horizontalGridLine',['../classfrechet_1_1fs_1_1Grid.html#a2945ad9ef0798c72ce0d627902260620',1,'frechet::fs::Grid']]],
  ['horizontalintervals',['horizontalIntervals',['../classfrechet_1_1k_1_1kAlgorithm.html#ad80be478b78d3372e6e03117c33b0ba9',1,'frechet::k::kAlgorithm']]]
];
