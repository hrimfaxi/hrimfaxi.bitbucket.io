var searchData=
[
  ['n',['n',['../structfrechet_1_1poly_1_1Algorithm_1_1CurveData.html#aa6c05baeecdd79e04afa31083a1776f0',1,'frechet::poly::Algorithm::CurveData']]],
  ['nblocks',['nblocks',['../ocl__prototype_8cpp.html#afff15892eabab3f6e4e9892ca17fff5d',1,'nblocks(mzd_t const *A):&#160;ocl_prototype.cpp'],['../ocl__prototype_8h.html#afff15892eabab3f6e4e9892ca17fff5d',1,'nblocks(mzd_t const *A):&#160;ocl_prototype.cpp']]],
  ['new_5fclmatrix',['new_clmatrix',['../namespacefrechet_1_1data.html#a7f743fe151612ba2835066d6fe483884',1,'frechet::data']]],
  ['new_5fclmatrix_5ft',['new_clmatrix_t',['../classfrechet_1_1data_1_1MatrixPool.html#aa2a6537bdc4d7bddf35e3b2e27d5d95e',1,'frechet::data::MatrixPool']]],
  ['new_5fmzd',['new_mzd',['../namespacefrechet_1_1data.html#af1b445e3ebe3ab0548a7110651d53bed',1,'frechet::data']]],
  ['new_5fmzd_5ft',['new_mzd_t',['../classfrechet_1_1data_1_1MatrixPool.html#afc6dba6ecd1fce31f6d63f9394b44504',1,'frechet::data::MatrixPool']]],
  ['newgraph',['newGraph',['../namespacefrechet_1_1reach.html#a4892c800dd182e96d9becddac1852cd9',1,'frechet::reach::newGraph(const GraphModel::ptr model)'],['../namespacefrechet_1_1reach.html#acc5bc19b9452d827f435cde77ced32fb',1,'frechet::reach::newGraph(const GraphModel::ptr model, IndexRange hmask)'],['../namespacefrechet_1_1reach.html#a2d5986719114ba44f191fcd36f259410',1,'frechet::reach::newGraph(const GraphModel::ptr model, Structure &amp;str)']]],
  ['newpolyalgorithm',['newPolyAlgorithm',['../namespacefrechet_1_1poly.html#a7eb5f0ce9294c02ca9280c4114832b1e',1,'frechet::poly']]],
  ['next',['next',['../classfrechet_1_1data_1_1BitSet.html#ad3a23d344160ecf954bee4a1b71c8cf5',1,'frechet::data::BitSet::next()'],['../classfrechet_1_1data_1_1BLinkedListElement.html#a3fac3750a543319360ac7b7e714d68a0',1,'frechet::data::BLinkedListElement::next()'],['../classfrechet_1_1data_1_1LinkedListElement.html#a09c410f765e5ba6dab6d465d4e41ad25',1,'frechet::data::LinkedListElement::next() const'],['../classfrechet_1_1data_1_1LinkedListElement.html#a1f5d0a6ec383d5daa4fe0c1e640ae31c',1,'frechet::data::LinkedListElement::next(bool forward)'],['../classfrechet_1_1reach_1_1Structure.html#aa1a27e73e855c5947e16c2196435be4a',1,'frechet::reach::Structure::next()']]],
  ['next_5fhor',['next_hor',['../classfrechet_1_1reach_1_1FSPath.html#aa3bd45d796298f74b83a908f7e6c9ef0',1,'frechet::reach::FSPath']]],
  ['next_5fvert',['next_vert',['../classfrechet_1_1reach_1_1FSPath.html#af3d19f76905e61e90ac9626b867c3679',1,'frechet::reach::FSPath']]],
  ['nextcolor',['nextColor',['../classfrechet_1_1view_1_1Palette.html#a6bb8e331b75e8a8ef41036414670e61e',1,'frechet::view::Palette']]],
  ['nextreachable',['nextReachable',['../classfrechet_1_1reach_1_1Structure.html#a27b36e3d5937a8a705ab10f044724064',1,'frechet::reach::Structure']]],
  ['normalize',['normalize',['../classfrechet_1_1data_1_1Interval.html#a4f11ee82a54525d846460c0888cfdc88',1,'frechet::data::Interval::normalize()'],['../classfrechet_1_1fs_1_1Components.html#a244177b20d75a84cfaa25a0dbda45778',1,'frechet::fs::Components::normalize()']]],
  ['normalized',['normalized',['../classfrechet_1_1data_1_1Interval.html#ab5f1a6654dfbe5f4c6e671a2c7bcc266',1,'frechet::data::Interval::normalized()'],['../structfrechet_1_1reach_1_1PointerInterval.html#a3546745e9f5d92c6caf5285814b53b09',1,'frechet::reach::PointerInterval::normalized()'],['../namespacefrechet_1_1numeric.html#a8e3525df21c95254839ba28653ccf5d5',1,'frechet::numeric::normalized()']]],
  ['normalizedvalue',['normalizedValue',['../baseview_8cpp.html#a3869e1c734950ea115e99b9f785b055c',1,'normalizedValue():&#160;baseview.cpp'],['../namespacefrechet_1_1view.html#aaa90c2a2490ecdb492d28bc79ee495fc',1,'frechet::view::normalizedValue()']]],
  ['numberofleadingzeros',['numberOfLeadingZeros',['../classfrechet_1_1data_1_1BitSet.html#a0d3468a5c3a0088f169c82dc1d626062',1,'frechet::data::BitSet']]],
  ['numberoftrailingzeros',['numberOfTrailingZeros',['../classfrechet_1_1data_1_1BitSet.html#aa16111e14c14b5bd0c37235c25cdae69',1,'frechet::data::BitSet']]]
];
