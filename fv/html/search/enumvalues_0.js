var searchData=
[
  ['add_5fleft',['ADD_LEFT',['../classfrechet_1_1poly_1_1DoubleEndedQueue.html#a01404db72f8582fb126e435fb6f97bd6a854970e8fcf70169fa087f632512a3ac',1,'frechet::poly::DoubleEndedQueue']]],
  ['add_5fright',['ADD_RIGHT',['../classfrechet_1_1poly_1_1DoubleEndedQueue.html#a01404db72f8582fb126e435fb6f97bd6a32830bd419a2d66f5e1a869b9aa1e475',1,'frechet::poly::DoubleEndedQueue']]],
  ['algorithm_5fcurve',['ALGORITHM_CURVE',['../classfrechet_1_1app_1_1FrechetViewApplication.html#adc6ca6d76602bbaa4b8f07fb1d299142af53d08478c1b4d4f5efb4ec17999ee22',1,'frechet::app::FrechetViewApplication']]],
  ['algorithm_5fk_5ffrechet',['ALGORITHM_K_FRECHET',['../classfrechet_1_1app_1_1FrechetViewApplication.html#adc6ca6d76602bbaa4b8f07fb1d299142a6a688d8e0568f51027d794a106f5bda9',1,'frechet::app::FrechetViewApplication']]],
  ['algorithm_5fpolygon',['ALGORITHM_POLYGON',['../classfrechet_1_1app_1_1FrechetViewApplication.html#adc6ca6d76602bbaa4b8f07fb1d299142a216de548956ccbd16afd4f68ba36ad87',1,'frechet::app::FrechetViewApplication']]],
  ['approx_5fgreene',['APPROX_GREENE',['../classfrechet_1_1poly_1_1PolygonUtilities.html#aa06c001515ddb3a69824ea68a1e03736a3e35d066c479aea8275c43293eccaeab',1,'frechet::poly::PolygonUtilities']]],
  ['approx_5fhertel',['APPROX_HERTEL',['../classfrechet_1_1poly_1_1PolygonUtilities.html#aa06c001515ddb3a69824ea68a1e03736abae0fee8a345e850925d4cc4ae861535',1,'frechet::poly::PolygonUtilities']]]
];
