var searchData=
[
  ['r',['R',['../classfrechet_1_1poly_1_1DoubleEndedQueue.html#aad56aa88e582b1153cfaf92ba56f046b',1,'frechet::poly::DoubleEndedQueue::R()'],['../classfrechet_1_1poly_1_1Vertex__base.html#aa12b1300a01c07fa302103515ae47949',1,'frechet::poly::Vertex_base::R()']]],
  ['reader',['reader',['../classfrechet_1_1app_1_1FrechetViewApplication.html#a0aecdd15f7874c3830c230a68aadc1e1',1,'frechet::app::FrechetViewApplication']]],
  ['reflex',['reflex',['../structfrechet_1_1poly_1_1Algorithm_1_1CurveData.html#a54e6dc066f9c775458ea56d645ddb458',1,'frechet::poly::Algorithm::CurveData']]],
  ['resetbutton',['resetButton',['../classfrechet_1_1view_1_1BaseView.html#a4229304ff93180081178b869a78004cd',1,'frechet::view::BaseView']]],
  ['result',['result',['../structfrechet_1_1k_1_1kAlgorithm_1_1Greedy.html#a857f487146b84a9a0707bc27f0464d8d',1,'frechet::k::kAlgorithm::Greedy::result()'],['../structfrechet_1_1k_1_1kAlgorithm_1_1BruteForce.html#a03f95b0e67604039d6e33c775f01e6d6',1,'frechet::k::kAlgorithm::BruteForce::result()']]],
  ['results',['results',['../classfrechet_1_1input_1_1InputReader.html#ada17ad072efc2b76afd932f7073ad3e2',1,'frechet::input::InputReader']]],
  ['reversed',['reversed',['../classfrechet_1_1reach_1_1GraphModel.html#a47b05ae55370e80f849ce361a8b47888',1,'frechet::reach::GraphModel']]],
  ['roots',['roots',['../classfrechet_1_1poly_1_1AlgorithmTopoSort.html#a39182fdb3bc27b679ea69c8d093bbe0f',1,'frechet::poly::AlgorithmTopoSort']]],
  ['rot0',['rot0',['../classfrechet_1_1view_1_1GraphicsView.html#a2788fdf904d61418eb3100921b49fc39',1,'frechet::view::GraphicsView']]],
  ['rotate_5fdefault_5fvalue',['ROTATE_DEFAULT_VALUE',['../classfrechet_1_1view_1_1BaseView.html#a111cdafad58d5ac6d853d19ea412109f',1,'frechet::view::BaseView']]],
  ['rotateslider',['rotateSlider',['../classfrechet_1_1view_1_1BaseView.html#a51d11c291b92d9f46ae7d7852d12d961',1,'frechet::view::BaseView']]],
  ['row_5fheight',['ROW_HEIGHT',['../classfrechet_1_1view_1_1IntervalView.html#a8ae801d81e3f25df0208b84d6d5bd307',1,'frechet::view::IntervalView']]],
  ['rr',['RR',['../classfrechet_1_1poly_1_1Vertex__base.html#a5d0c60a543e941d62159c62a929fd528',1,'frechet::poly::Vertex_base']]]
];
