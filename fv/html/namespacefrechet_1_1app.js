var namespacefrechet_1_1app =
[
    [ "ConcurrencyContext", "classfrechet_1_1app_1_1ConcurrencyContext.html", "classfrechet_1_1app_1_1ConcurrencyContext" ],
    [ "FileEntry", "structfrechet_1_1app_1_1FileEntry.html", "structfrechet_1_1app_1_1FileEntry" ],
    [ "FileHistory", "classfrechet_1_1app_1_1FileHistory.html", "classfrechet_1_1app_1_1FileHistory" ],
    [ "FrechetViewApplication", "classfrechet_1_1app_1_1FrechetViewApplication.html", "classfrechet_1_1app_1_1FrechetViewApplication" ],
    [ "InterruptedException", "classfrechet_1_1app_1_1InterruptedException.html", null ],
    [ "WorkerJob", "classfrechet_1_1app_1_1WorkerJob.html", "classfrechet_1_1app_1_1WorkerJob" ],
    [ "WorkerJobHandle", "classfrechet_1_1app_1_1WorkerJobHandle.html", "classfrechet_1_1app_1_1WorkerJobHandle" ]
];