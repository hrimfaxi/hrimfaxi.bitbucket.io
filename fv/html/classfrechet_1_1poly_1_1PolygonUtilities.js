var classfrechet_1_1poly_1_1PolygonUtilities =
[
    [ "DecompositionAlgorithm", "classfrechet_1_1poly_1_1PolygonUtilities.html#aa06c001515ddb3a69824ea68a1e03736", [
      [ "APPROX_GREENE", "classfrechet_1_1poly_1_1PolygonUtilities.html#aa06c001515ddb3a69824ea68a1e03736a3e35d066c479aea8275c43293eccaeab", null ],
      [ "APPROX_HERTEL", "classfrechet_1_1poly_1_1PolygonUtilities.html#aa06c001515ddb3a69824ea68a1e03736abae0fee8a345e850925d4cc4ae861535", null ],
      [ "OPTIMAL_GREENE", "classfrechet_1_1poly_1_1PolygonUtilities.html#aa06c001515ddb3a69824ea68a1e03736ac2c200ca4afb855a2cc9d567d2e76afb", null ]
    ] ],
    [ "PolygonUtilities", "classfrechet_1_1poly_1_1PolygonUtilities.html#a846cc9005961b95d1f094918d3890946", null ],
    [ "PolygonUtilities", "classfrechet_1_1poly_1_1PolygonUtilities.html#a10f33cdbc8257776e33a8b952c2262b0", null ],
    [ "assertStrictlySorted", "classfrechet_1_1poly_1_1PolygonUtilities.html#a7ad9fc006a1f50ccfd971e255b9f5433", null ],
    [ "create1Diagonal", "classfrechet_1_1poly_1_1PolygonUtilities.html#a347beea7d3a448e7dd6dea18bc3f6b75", null ],
    [ "decompose", "classfrechet_1_1poly_1_1PolygonUtilities.html#a0605bf7563d6f8ec5c25b31b420334dc", null ],
    [ "fanTriangulate", "classfrechet_1_1poly_1_1PolygonUtilities.html#aed54b639dd773b6150932f2cf40ef16a", null ],
    [ "fanTriangulate", "classfrechet_1_1poly_1_1PolygonUtilities.html#afbd063fbde18d193e05804deb667cad5", null ],
    [ "findReflexVertices", "classfrechet_1_1poly_1_1PolygonUtilities.html#a85a926bb97d0583b04ba615a2d2cbf02", null ],
    [ "is_convex", "classfrechet_1_1poly_1_1PolygonUtilities.html#ad8721d866033ee2543b5c575ebab1bf9", null ],
    [ "is_counter_clockwise", "classfrechet_1_1poly_1_1PolygonUtilities.html#a3576a0b279691946a2ce71accec7ea92", null ],
    [ "is_reflex_vertex", "classfrechet_1_1poly_1_1PolygonUtilities.html#a0e1d702ecd6720f044d96b743ed17098", null ],
    [ "is_simple", "classfrechet_1_1poly_1_1PolygonUtilities.html#a782991e198b3f2fc166f4d4344fe92e5", null ],
    [ "partition", "classfrechet_1_1poly_1_1PolygonUtilities.html#af67b429b5abc7154babe29489deff4cd", null ],
    [ "simplify", "classfrechet_1_1poly_1_1PolygonUtilities.html#acd7ef5bac5eb2688e900d7c4263ac5c5", null ],
    [ "simplify", "classfrechet_1_1poly_1_1PolygonUtilities.html#aaded9f0db2784d3d9272a1de398710b5", null ],
    [ "cgp", "classfrechet_1_1poly_1_1PolygonUtilities.html#ad3a5a265783bc4eed70e6b7443629a4e", null ],
    [ "convex", "classfrechet_1_1poly_1_1PolygonUtilities.html#a581dc8f7b04b808f035898a9ec8f87da", null ]
];