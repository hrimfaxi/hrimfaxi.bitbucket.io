var classfrechet_1_1view_1_1Palette =
[
    [ "ColorMap", "classfrechet_1_1view_1_1Palette.html#a929b1fced209ef5c2feb7b77da147265", null ],
    [ "Palette", "classfrechet_1_1view_1_1Palette.html#a6eb6cac583a840b5251897e3403eac5a", null ],
    [ "addLightness", "classfrechet_1_1view_1_1Palette.html#a7c19a102d4906af68b0e78d085b41add", null ],
    [ "clear", "classfrechet_1_1view_1_1Palette.html#aabc53a36cbf3e091f2a5f79f52208a45", null ],
    [ "nextColor", "classfrechet_1_1view_1_1Palette.html#a6bb8e331b75e8a8ef41036414670e61e", null ],
    [ "operator[]", "classfrechet_1_1view_1_1Palette.html#a1da5a12dfbaee049895551ef97fe82db", null ],
    [ "randomColor", "classfrechet_1_1view_1_1Palette.html#ae1ab905f05f1e8d89021e322b91b5889", null ],
    [ "stronger", "classfrechet_1_1view_1_1Palette.html#a9652745bc61d916ddf25128c0346e1c0", null ],
    [ "toColor", "classfrechet_1_1view_1_1Palette.html#ac4fa46716526d5a0912bcc3baf3fe35b", null ],
    [ "weaker", "classfrechet_1_1view_1_1Palette.html#a60902d6bc20aa600a2898544fe9bfa98", null ],
    [ "map", "classfrechet_1_1view_1_1Palette.html#a4bfa6fa7621250c2305afc6c323ce1a6", null ]
];