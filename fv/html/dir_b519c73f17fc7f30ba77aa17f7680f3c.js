var dir_b519c73f17fc7f30ba77aa17f7680f3c =
[
    [ "baseview.h", "baseview_8h.html", "baseview_8h" ],
    [ "controlpanel.h", "controlpanel_8h.html", "controlpanel_8h" ],
    [ "curveview.h", "curveview_8h.html", [
      [ "CurveView", "classfrechet_1_1view_1_1CurveView.html", "classfrechet_1_1view_1_1CurveView" ]
    ] ],
    [ "freespaceview.h", "freespaceview_8h.html", [
      [ "CellView", "classfrechet_1_1view_1_1CellView.html", "classfrechet_1_1view_1_1CellView" ],
      [ "FreeSpaceView", "classfrechet_1_1view_1_1FreeSpaceView.html", "classfrechet_1_1view_1_1FreeSpaceView" ]
    ] ],
    [ "intervalview.h", "intervalview_8h.html", [
      [ "IntervalView", "classfrechet_1_1view_1_1IntervalView.html", "classfrechet_1_1view_1_1IntervalView" ]
    ] ],
    [ "mainwindow.h", "mainwindow_8h.html", [
      [ "MainWindow", "classfrechet_1_1view_1_1MainWindow.html", "classfrechet_1_1view_1_1MainWindow" ]
    ] ],
    [ "palette.h", "palette_8h.html", [
      [ "Palette", "classfrechet_1_1view_1_1Palette.html", "classfrechet_1_1view_1_1Palette" ]
    ] ]
];