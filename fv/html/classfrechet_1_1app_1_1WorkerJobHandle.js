var classfrechet_1_1app_1_1WorkerJobHandle =
[
    [ "WorkerJobHandle", "classfrechet_1_1app_1_1WorkerJobHandle.html#a71512cea16a07af821f5045ff5c68748", null ],
    [ "cancelJob", "classfrechet_1_1app_1_1WorkerJobHandle.html#a9f88d441ad33ce6d331bb0889b514346", null ],
    [ "invalidate", "classfrechet_1_1app_1_1WorkerJobHandle.html#a97f56904c4d0fa066979486cc4655fdf", null ],
    [ "shutDown", "classfrechet_1_1app_1_1WorkerJobHandle.html#a8af35c0731545bd67704bb6a095850c4", null ],
    [ "startJob", "classfrechet_1_1app_1_1WorkerJobHandle.html#a72a226c967a4a3d143d2cd496a599858", null ],
    [ "job", "classfrechet_1_1app_1_1WorkerJobHandle.html#a2ddb168ed0f5554506645372aa1946d1", null ],
    [ "mut", "classfrechet_1_1app_1_1WorkerJobHandle.html#a6541a99d21c6ea716738fa7749f398e9", null ]
];