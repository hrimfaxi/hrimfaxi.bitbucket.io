var clm4rm_8cpp =
[
    [ "str", "clm4rm_8cpp.html#ae2ce8fc94b09d4cbb836697dfe65614a", null ],
    [ "assertMatrixLayout", "clm4rm_8cpp.html#a80654f22a7d0a5aa8eb9cda0c8fb91f2", null ],
    [ "build_program", "clm4rm_8cpp.html#a3385697abdb23c3a2a07d2ef1dab2639", null ],
    [ "clm4rm_allocate", "clm4rm_8cpp.html#a7ef9686cb74daba1beceb9aa19d61b4f", null ],
    [ "clm4rm_copy", "clm4rm_8cpp.html#a2f3a974934ea9400d27df895eed7affb", null ],
    [ "clm4rm_create", "clm4rm_8cpp.html#ac16748f1e5acb851d7fb54ef2306c002", null ],
    [ "clm4rm_read", "clm4rm_8cpp.html#a8d793dd80a1be59356524d46377a50bf", null ],
    [ "clm4rm_setup", "clm4rm_8cpp.html#af4e4e006304db73c4cfdfccc2d2dbec6", null ],
    [ "clm4rm_tear_down", "clm4rm_8cpp.html#ac1eb06341644b1b9cead588733cf22ce", null ],
    [ "clm4rm_write", "clm4rm_8cpp.html#aabba54a2f622382af265968b6dbf8c34", null ],
    [ "clm4rm_zero_fill", "clm4rm_8cpp.html#a13248ea72a3a18ba22972424ce5982fd", null ],
    [ "copy_back_matrix_data", "clm4rm_8cpp.html#afeca25ea8bf400f62891f8428e448fce", null ],
    [ "copy_matrix_data", "clm4rm_8cpp.html#aa24a85005673172c2ee410c592c0af49", null ],
    [ "init_conditions", "clm4rm_8cpp.html#afb2bd7cdf5a246a8228830432b8c4771", null ],
    [ "init_events", "clm4rm_8cpp.html#a6429f615e235d08e35f0e21ccbdf084b", null ],
    [ "join_conditions", "clm4rm_8cpp.html#ac131afe8b1a7824e2f07943beb37f5b8", null ],
    [ "load_program", "clm4rm_8cpp.html#a11d811a25daf20f8c38d956943ca0dde", null ],
    [ "merge_conditions", "clm4rm_8cpp.html#a054d1dd2203f7b14901d8ed703a43dd4", null ],
    [ "merge_events", "clm4rm_8cpp.html#ab5c37cd4b1ec39ad45e4b85e8558efbd", null ],
    [ "padded_rows", "clm4rm_8cpp.html#a00b6bbc3f72d06745c9ba582d12a1129", null ],
    [ "pre_count", "clm4rm_8cpp.html#af80899953be201a5b9c44b68ab0fe5a2", null ],
    [ "pre_events", "clm4rm_8cpp.html#a595061ae2387e6111805ef4ac80940b5", null ],
    [ "push_event", "clm4rm_8cpp.html#af6cc346783c31a24ae0d7bda9a4cb12a", null ],
    [ "pushed_event", "clm4rm_8cpp.html#a45f0989dd9f7bf427efcd366bede443a", null ],
    [ "release_conditions", "clm4rm_8cpp.html#aae93b5f26b6c68bb7c001e66d38f27e8", null ],
    [ "release_events", "clm4rm_8cpp.html#af619bc0ec23d358fabb42f9b89baab5a", null ],
    [ "track_heap_size", "clm4rm_8cpp.html#a9d5c3d4ac7f60188b6e223bf690bfaee", null ],
    [ "allocated_size", "clm4rm_8cpp.html#af31444d597c4a2dfb5e543712cb57d3d", null ],
    [ "clcubic_mul_kernel", "clm4rm_8cpp.html#a0220f5e41f76a4a8313e3e734a793431", null ],
    [ "clm4rm_and_kernel", "clm4rm_8cpp.html#ad109841605c2500f5b6132e4e5d688dd", null ],
    [ "clm4rm_copy_kernel", "clm4rm_8cpp.html#a817d1adb3caef5f209feb620977b707d", null ],
    [ "clm4rm_error", "clm4rm_8cpp.html#aba19d305d3352e15ff5f919c3272e174", null ],
    [ "clm4rm_mul_kernel", "clm4rm_8cpp.html#a907a1414ddd6b750213c916f6835b082", null ],
    [ "clm4rm_or_kernel", "clm4rm_8cpp.html#a486d512e9449c3383da7746f35f0fa4b", null ],
    [ "clm4rm_query_diagonal_kernel", "clm4rm_8cpp.html#a3e7c3ea69b50d91eb18e0dfb40ca055b", null ],
    [ "clutri_mul_kernel", "clm4rm_8cpp.html#acd51b4170a165d39065036f86c602201", null ],
    [ "heap_size", "clm4rm_8cpp.html#a19d898063f34e7e60d8a4689d1c6c2bb", null ],
    [ "IMAGE_FORMAT", "clm4rm_8cpp.html#ad1b650925709ca50bd624a2c7c0f723e", null ],
    [ "max_group_size", "clm4rm_8cpp.html#ac90a4e4ea827b40e2035038244e9f3a8", null ],
    [ "max_items", "clm4rm_8cpp.html#ac39d2c291d9ece0d8055a02069f67534", null ],
    [ "max_object_size", "clm4rm_8cpp.html#ad9c430255baa8f734310e1f083e8b6d8", null ],
    [ "printed_heap_warning", "clm4rm_8cpp.html#afd74b4fc7f54354591414c0d22b8bc5c", null ],
    [ "programs", "clm4rm_8cpp.html#a00b9e956495e22e5326e1f86d7c4a022", null ],
    [ "shared_mem_bytes", "clm4rm_8cpp.html#a9424b4cd81d8fdec350d3e217934a91a", null ],
    [ "shared_mem_words", "clm4rm_8cpp.html#af9e923a3cc3c40b2e3a0e316ff8d2eec", null ]
];