var frechet_2poly_2types_8h =
[
    [ "Segment", "classfrechet_1_1poly_1_1Segment.html", "classfrechet_1_1poly_1_1Segment" ],
    [ "Partition", "frechet_2poly_2types_8h.html#a1ae68423d0da80b9291f3ac65af21668", null ],
    [ "Polygon", "frechet_2poly_2types_8h.html#a7743f48b3218f02565db3dbf56743378", null ],
    [ "Segments", "frechet_2poly_2types_8h.html#a91453b6b71090aca3fc7f07941a24cc8", null ],
    [ "operator<<", "frechet_2poly_2types_8h.html#a21b2f5b85d29cde0f566c7f71d6896e5", null ],
    [ "SOUTH_POLE_INDEX", "frechet_2poly_2types_8h.html#a54706163500f7e02f0237a144a5aff77", null ]
];