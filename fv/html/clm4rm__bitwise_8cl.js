var clm4rm__bitwise_8cl =
[
    [ "B_nrows", "clm4rm__bitwise_8cl.html#a2d3313e160594a88683a7be99106fa3d", null ],
    [ "C_nrows", "clm4rm__bitwise_8cl.html#a5e5cd88cfce860c1cc4ea0f3162f172c", null ],
    [ "read", "clm4rm__bitwise_8cl.html#ab1ec6badaab75a0658ce6c558c2d0a0b", null ],
    [ "read_only_global", "clm4rm__bitwise_8cl.html#a37296a53c91f3d8e7ecdc7f65d0c580e", null ],
    [ "write", "clm4rm__bitwise_8cl.html#af7598ce4f02e426e352754d34c72ba72", null ],
    [ "WRITE_ATOMIC", "clm4rm__bitwise_8cl.html#a3ac961a32c4f5668271b005cd2db4d06", null ],
    [ "write_only_global", "clm4rm__bitwise_8cl.html#a447956856dff61d28b6973823efcba57", null ],
    [ "gpuword", "clm4rm__bitwise_8cl.html#aa4dfe7b3bc33c28eac7f63fe6174f59c", null ],
    [ "clm4rm_and", "clm4rm__bitwise_8cl.html#a85278ed778037655bc142404e9a3e59d", null ],
    [ "clm4rm_copy", "clm4rm__bitwise_8cl.html#ab548051c292cff6ed70c0969e83db377", null ],
    [ "clm4rm_or", "clm4rm__bitwise_8cl.html#a0a13a90cba3dc2c243bd90d8bb1e9720", null ],
    [ "clm4rm_query_diagonal", "clm4rm__bitwise_8cl.html#a90a9879356dc66c0bfe1f0251a736bde", null ]
];