var namespacefrechet_1_1view =
[
    [ "BaseView", "classfrechet_1_1view_1_1BaseView.html", "classfrechet_1_1view_1_1BaseView" ],
    [ "CellView", "classfrechet_1_1view_1_1CellView.html", "classfrechet_1_1view_1_1CellView" ],
    [ "ControlPanel", "classfrechet_1_1view_1_1ControlPanel.html", "classfrechet_1_1view_1_1ControlPanel" ],
    [ "CurveView", "classfrechet_1_1view_1_1CurveView.html", "classfrechet_1_1view_1_1CurveView" ],
    [ "FreeSpaceView", "classfrechet_1_1view_1_1FreeSpaceView.html", "classfrechet_1_1view_1_1FreeSpaceView" ],
    [ "GraphicsHoverLineItem", "classfrechet_1_1view_1_1GraphicsHoverLineItem.html", "classfrechet_1_1view_1_1GraphicsHoverLineItem" ],
    [ "GraphicsView", "classfrechet_1_1view_1_1GraphicsView.html", "classfrechet_1_1view_1_1GraphicsView" ],
    [ "IntervalView", "classfrechet_1_1view_1_1IntervalView.html", "classfrechet_1_1view_1_1IntervalView" ],
    [ "MainWindow", "classfrechet_1_1view_1_1MainWindow.html", "classfrechet_1_1view_1_1MainWindow" ],
    [ "Palette", "classfrechet_1_1view_1_1Palette.html", "classfrechet_1_1view_1_1Palette" ]
];