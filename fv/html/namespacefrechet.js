var namespacefrechet =
[
    [ "app", "namespacefrechet_1_1app.html", "namespacefrechet_1_1app" ],
    [ "data", "namespacefrechet_1_1data.html", "namespacefrechet_1_1data" ],
    [ "fs", "namespacefrechet_1_1fs.html", "namespacefrechet_1_1fs" ],
    [ "input", "namespacefrechet_1_1input.html", "namespacefrechet_1_1input" ],
    [ "k", "namespacefrechet_1_1k.html", "namespacefrechet_1_1k" ],
    [ "poly", "namespacefrechet_1_1poly.html", "namespacefrechet_1_1poly" ],
    [ "reach", "namespacefrechet_1_1reach.html", "namespacefrechet_1_1reach" ],
    [ "view", "namespacefrechet_1_1view.html", "namespacefrechet_1_1view" ]
];