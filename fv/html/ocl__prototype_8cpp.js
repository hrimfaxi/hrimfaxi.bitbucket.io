var ocl__prototype_8cpp =
[
    [ "adjust_k", "ocl__prototype_8cpp.html#ad7d3e7a8f0b6468b94aa6cccda238860", null ],
    [ "assertEquals", "ocl__prototype_8cpp.html#a2a96f914a4d78a58ba08e80028b6a391", null ],
    [ "combinate", "ocl__prototype_8cpp.html#a83063d62cd783dbf69e7adf63c618226", null ],
    [ "nblocks", "ocl__prototype_8cpp.html#afff15892eabab3f6e4e9892ca17fff5d", null ],
    [ "proto_bool_mul_cubic", "ocl__prototype_8cpp.html#a642e0dee7f8c3f5be370f4ecbc6b8e2b", null ],
    [ "proto_bool_mul_m4rm", "ocl__prototype_8cpp.html#a2337bc900cce1144d4ed9b44dda430f5", null ],
    [ "proto_mul_cubic", "ocl__prototype_8cpp.html#af6be44ffa75a8a5c3571141c5339fed2", null ],
    [ "proto_mul_m4rm", "ocl__prototype_8cpp.html#a647e2516f5eeaa5f26a95a0ad5bf3807", null ],
    [ "proto_mul_m4rm_block", "ocl__prototype_8cpp.html#a6e6ea5d4c1d1dea72e639e658435a141", null ],
    [ "proto_read_bits", "ocl__prototype_8cpp.html#a4a532ca5efc45b37e330e1708bcd7976", null ],
    [ "swap", "ocl__prototype_8cpp.html#a528d24840b5e6deab5c20dc02731430c", null ]
];