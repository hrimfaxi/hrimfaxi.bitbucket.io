var dir_bd52eb7ac821349adc834157bbaf2648 =
[
    [ "array2d.h", "array2d_8h.html", [
      [ "Array2D", "classfrechet_1_1data_1_1Array2D.html", "classfrechet_1_1data_1_1Array2D" ],
      [ "iterator", "classfrechet_1_1data_1_1Array2D_1_1iterator.html", "classfrechet_1_1data_1_1Array2D_1_1iterator" ]
    ] ],
    [ "array2d_impl.h", "array2d__impl_8h.html", null ],
    [ "bitset.h", "bitset_8h.html", [
      [ "BitSet", "classfrechet_1_1data_1_1BitSet.html", "classfrechet_1_1data_1_1BitSet" ],
      [ "iterator", "classfrechet_1_1data_1_1BitSet_1_1iterator.html", "classfrechet_1_1data_1_1BitSet_1_1iterator" ]
    ] ],
    [ "interval.h", "interval_8h.html", "interval_8h" ],
    [ "linkedlist.h", "linkedlist_8h.html", [
      [ "BLinkedListElement", "classfrechet_1_1data_1_1BLinkedListElement.html", "classfrechet_1_1data_1_1BLinkedListElement" ],
      [ "LinkedListElement", "classfrechet_1_1data_1_1LinkedListElement.html", "classfrechet_1_1data_1_1LinkedListElement" ],
      [ "BLinkedList", "classfrechet_1_1data_1_1BLinkedList.html", "classfrechet_1_1data_1_1BLinkedList" ],
      [ "LinkedList", "classfrechet_1_1data_1_1LinkedList.html", "classfrechet_1_1data_1_1LinkedList" ]
    ] ],
    [ "matrix_pool.h", "matrix__pool_8h.html", "matrix__pool_8h" ],
    [ "numeric.h", "numeric_8h.html", "numeric_8h" ],
    [ "spirolator.h", "spirolator_8h.html", [
      [ "Spirolator", "classfrechet_1_1data_1_1Spirolator.html", "classfrechet_1_1data_1_1Spirolator" ]
    ] ],
    [ "types.h", "data_2types_8h.html", "data_2types_8h" ]
];