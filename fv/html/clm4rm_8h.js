var clm4rm_8h =
[
    [ "clmatrix_t", "structclmatrix__t.html", "structclmatrix__t" ],
    [ "clm4rm_event_list", "structclm4rm__event__list.html", "structclm4rm__event__list" ],
    [ "clm4rm_conditions", "structclm4rm__conditions.html", "structclm4rm__conditions" ],
    [ "BUFFERED", "clm4rm_8h.html#a87d5be05600f8494d9c995f01421a3a3", null ],
    [ "CEILCOLS", "clm4rm_8h.html#af5dd6be1c996bc5c4953b230549b5b10", null ],
    [ "CEILDIV", "clm4rm_8h.html#a04207b767bea5b6a8fcd117bbaf9900c", null ],
    [ "clm4rm_radix", "clm4rm_8h.html#aecab42a8b1ad86b3c083941848e6cfa1", null ],
    [ "DATA_BYTES", "clm4rm_8h.html#aa0cfe6d0157c466ba7580cea7b9bb9b0", null ],
    [ "FLOOR", "clm4rm_8h.html#a961b8c065f638db25e2dd88269107edb", null ],
    [ "IMAGE2D", "clm4rm_8h.html#a8f98fe572945009d545204f94ce60bb0", null ],
    [ "MAX_EVENTS", "clm4rm_8h.html#ae42954bb8545d24e3e9dcde5920c9a0b", null ],
    [ "MAX_TILE_M", "clm4rm_8h.html#a8ffd277fdf383f9982616c6cd7d58166", null ],
    [ "POW2", "clm4rm_8h.html#a943beb62b7b28ccafb40be892e05851f", null ],
    [ "clm4rm_conditions", "clm4rm_8h.html#a06104f7d2b164c220efc10da08e42d7d", null ],
    [ "clm4rm_event_list", "clm4rm_8h.html#a6150915cfaf5d0640c8eb124008b91ae", null ],
    [ "clmatrix_t", "clm4rm_8h.html#aab471d245a0fc2c82ca199a78887a3f8", null ],
    [ "gpuword", "clm4rm_8h.html#a86f1e55f1fd7ba4419f75091733e72ec", null ],
    [ "size2_t", "clm4rm_8h.html#a554f66d12b9c68f8d082727849764889", null ],
    [ "clcubic_mul", "clm4rm_8h.html#aa869afdf569a313d8329cdf153487f9f", null ],
    [ "clm4rm_and", "clm4rm_8h.html#ae244e55591f5e3af1e737b950217c42f", null ],
    [ "clm4rm_concat", "clm4rm_8h.html#a115ef17417b1e008bddf9657877c33c2", null ],
    [ "clm4rm_copy", "clm4rm_8h.html#a2f3a974934ea9400d27df895eed7affb", null ],
    [ "clm4rm_create", "clm4rm_8h.html#ac16748f1e5acb851d7fb54ef2306c002", null ],
    [ "clm4rm_free", "clm4rm_8h.html#ab793c0f37003902d3e69c589882a9be7", null ],
    [ "clm4rm_mul", "clm4rm_8h.html#a7ed3af53e10cf5780b9c7b203d0d3b9f", null ],
    [ "clm4rm_or", "clm4rm_8h.html#a6923350f0d51bb99bf70187eab539c35", null ],
    [ "clm4rm_query_diagonal", "clm4rm_8h.html#afc9e1721b58ea2b31d327d8134d52502", null ],
    [ "clm4rm_query_result", "clm4rm_8h.html#a3e90af2dd57c5bf70acf8ba49b0c7ec8", null ],
    [ "clm4rm_read", "clm4rm_8h.html#a8d793dd80a1be59356524d46377a50bf", null ],
    [ "clm4rm_setup", "clm4rm_8h.html#af4e4e006304db73c4cfdfccc2d2dbec6", null ],
    [ "clm4rm_stack", "clm4rm_8h.html#ae972c8b9033022b01f170b11a683a1e3", null ],
    [ "clm4rm_tear_down", "clm4rm_8h.html#ac1eb06341644b1b9cead588733cf22ce", null ],
    [ "clm4rm_write", "clm4rm_8h.html#aabba54a2f622382af265968b6dbf8c34", null ],
    [ "clm4rm_zero_fill", "clm4rm_8h.html#a13248ea72a3a18ba22972424ce5982fd", null ],
    [ "clutri_mul", "clm4rm_8h.html#a1bed37a0d226d8f186353c8e9e1c433c", null ],
    [ "copy_back_matrix_data", "clm4rm_8h.html#a9754d6942da0e514f4644af163d353bc", null ],
    [ "copy_matrix_data", "clm4rm_8h.html#a1d269902a8ce7d516666e6ad9c366dec", null ],
    [ "init_conditions", "clm4rm_8h.html#afb2bd7cdf5a246a8228830432b8c4771", null ],
    [ "init_events", "clm4rm_8h.html#a6429f615e235d08e35f0e21ccbdf084b", null ],
    [ "join_conditions", "clm4rm_8h.html#ac131afe8b1a7824e2f07943beb37f5b8", null ],
    [ "merge_conditions", "clm4rm_8h.html#a054d1dd2203f7b14901d8ed703a43dd4", null ],
    [ "merge_events", "clm4rm_8h.html#ab5c37cd4b1ec39ad45e4b85e8558efbd", null ],
    [ "padded_rows", "clm4rm_8h.html#a00b6bbc3f72d06745c9ba582d12a1129", null ],
    [ "pre_count", "clm4rm_8h.html#af80899953be201a5b9c44b68ab0fe5a2", null ],
    [ "pre_events", "clm4rm_8h.html#a595061ae2387e6111805ef4ac80940b5", null ],
    [ "push_event", "clm4rm_8h.html#af6cc346783c31a24ae0d7bda9a4cb12a", null ],
    [ "pushed_event", "clm4rm_8h.html#a45f0989dd9f7bf427efcd366bede443a", null ],
    [ "release_conditions", "clm4rm_8h.html#aae93b5f26b6c68bb7c001e66d38f27e8", null ],
    [ "release_events", "clm4rm_8h.html#af619bc0ec23d358fabb42f9b89baab5a", null ],
    [ "allocated_size", "clm4rm_8h.html#af31444d597c4a2dfb5e543712cb57d3d", null ],
    [ "clm4rm_error", "clm4rm_8h.html#aba19d305d3352e15ff5f919c3272e174", null ],
    [ "heap_size", "clm4rm_8h.html#a19d898063f34e7e60d8a4689d1c6c2bb", null ],
    [ "max_group_size", "clm4rm_8h.html#ac90a4e4ea827b40e2035038244e9f3a8", null ],
    [ "max_items", "clm4rm_8h.html#ac39d2c291d9ece0d8055a02069f67534", null ],
    [ "max_object_size", "clm4rm_8h.html#ad9c430255baa8f734310e1f083e8b6d8", null ],
    [ "shared_mem_bytes", "clm4rm_8h.html#a9424b4cd81d8fdec350d3e217934a91a", null ],
    [ "shared_mem_words", "clm4rm_8h.html#af9e923a3cc3c40b2e3a0e316ff8d2eec", null ]
];