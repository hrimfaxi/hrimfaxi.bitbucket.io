var namespacefrechet_1_1data =
[
    [ "Array2D", "classfrechet_1_1data_1_1Array2D.html", "classfrechet_1_1data_1_1Array2D" ],
    [ "BitSet", "classfrechet_1_1data_1_1BitSet.html", "classfrechet_1_1data_1_1BitSet" ],
    [ "BLinkedList", "classfrechet_1_1data_1_1BLinkedList.html", "classfrechet_1_1data_1_1BLinkedList" ],
    [ "BLinkedListElement", "classfrechet_1_1data_1_1BLinkedListElement.html", "classfrechet_1_1data_1_1BLinkedListElement" ],
    [ "Interval", "classfrechet_1_1data_1_1Interval.html", "classfrechet_1_1data_1_1Interval" ],
    [ "IntervalPair", "classfrechet_1_1data_1_1IntervalPair.html", "classfrechet_1_1data_1_1IntervalPair" ],
    [ "LinkedList", "classfrechet_1_1data_1_1LinkedList.html", "classfrechet_1_1data_1_1LinkedList" ],
    [ "LinkedListElement", "classfrechet_1_1data_1_1LinkedListElement.html", "classfrechet_1_1data_1_1LinkedListElement" ],
    [ "MatrixPool", "classfrechet_1_1data_1_1MatrixPool.html", "classfrechet_1_1data_1_1MatrixPool" ],
    [ "Rect", "structfrechet_1_1data_1_1Rect.html", "structfrechet_1_1data_1_1Rect" ],
    [ "Spirolator", "classfrechet_1_1data_1_1Spirolator.html", "classfrechet_1_1data_1_1Spirolator" ]
];