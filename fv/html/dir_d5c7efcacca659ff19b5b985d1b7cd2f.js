var dir_d5c7efcacca659ff19b5b985d1b7cd2f =
[
    [ "concurrency.h", "concurrency_8h.html", "concurrency_8h" ],
    [ "filehistory.h", "filehistory_8h.html", [
      [ "FileEntry", "structfrechet_1_1app_1_1FileEntry.html", "structfrechet_1_1app_1_1FileEntry" ],
      [ "FileHistory", "classfrechet_1_1app_1_1FileHistory.html", "classfrechet_1_1app_1_1FileHistory" ]
    ] ],
    [ "frechetviewapplication.h", "frechetviewapplication_8h.html", [
      [ "FrechetViewApplication", "classfrechet_1_1app_1_1FrechetViewApplication.html", "classfrechet_1_1app_1_1FrechetViewApplication" ]
    ] ],
    [ "workerthread.h", "workerthread_8h.html", [
      [ "InterruptedException", "classfrechet_1_1app_1_1InterruptedException.html", null ],
      [ "WorkerJob", "classfrechet_1_1app_1_1WorkerJob.html", "classfrechet_1_1app_1_1WorkerJob" ],
      [ "WorkerJobHandle", "classfrechet_1_1app_1_1WorkerJobHandle.html", "classfrechet_1_1app_1_1WorkerJobHandle" ]
    ] ]
];