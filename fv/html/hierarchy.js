var hierarchy =
[
    [ "frechet::data::Array2D< T >", "classfrechet_1_1data_1_1Array2D.html", null ],
    [ "frechet::data::Array2D< data::IntervalPair >", "classfrechet_1_1data_1_1Array2D.html", null ],
    [ "frechet::data::Array2D< frechet::data::Interval >", "classfrechet_1_1data_1_1Array2D.html", null ],
    [ "frechet::data::Array2D< frechet::fs::Cell >", "classfrechet_1_1data_1_1Array2D.html", null ],
    [ "frechet::data::BitSet", "classfrechet_1_1data_1_1BitSet.html", null ],
    [ "frechet::data::BLinkedList", "classfrechet_1_1data_1_1BLinkedList.html", [
      [ "frechet::data::LinkedList< T >", "classfrechet_1_1data_1_1LinkedList.html", null ],
      [ "frechet::data::LinkedList< BoundarySegment >", "classfrechet_1_1data_1_1LinkedList.html", null ]
    ] ],
    [ "frechet::data::BLinkedListElement", "classfrechet_1_1data_1_1BLinkedListElement.html", [
      [ "frechet::data::LinkedListElement< T >", "classfrechet_1_1data_1_1LinkedListElement.html", null ],
      [ "frechet::data::LinkedListElement< BoundarySegment >", "classfrechet_1_1data_1_1LinkedListElement.html", [
        [ "frechet::reach::BoundarySegment", "classfrechet_1_1reach_1_1BoundarySegment.html", null ]
      ] ]
    ] ],
    [ "frechet::reach::BoundsIndex", "structfrechet_1_1reach_1_1BoundsIndex.html", null ],
    [ "frechet::k::kAlgorithm::BruteForce", "structfrechet_1_1k_1_1kAlgorithm_1_1BruteForce.html", null ],
    [ "frechet::fs::Cell", "classfrechet_1_1fs_1_1Cell.html", null ],
    [ "clm4rm_conditions", "structclm4rm__conditions.html", null ],
    [ "clm4rm_event_list", "structclm4rm__event__list.html", null ],
    [ "clmatrix_t", "structclmatrix__t.html", null ],
    [ "frechet::fs::Components", "classfrechet_1_1fs_1_1Components.html", null ],
    [ "frechet::app::ConcurrencyContext", "classfrechet_1_1app_1_1ConcurrencyContext.html", null ],
    [ "frechet::poly::Algorithm::CurveData", "structfrechet_1_1poly_1_1Algorithm_1_1CurveData.html", null ],
    [ "frechet::input::DataPath", "classfrechet_1_1input_1_1DataPath.html", null ],
    [ "frechet::poly::DoubleEndedQueue< T >", "classfrechet_1_1poly_1_1DoubleEndedQueue.html", null ],
    [ "frechet::poly::DoubleEndedQueue< Vertex_handle >", "classfrechet_1_1poly_1_1DoubleEndedQueue.html", null ],
    [ "frechet::poly::DummyTDS", "structfrechet_1_1poly_1_1DummyTDS.html", null ],
    [ "Edge_iterator", null, [
      [ "frechet::poly::Triangulation::Edge_iterator", "classfrechet_1_1poly_1_1Triangulation_1_1Edge__iterator.html", null ]
    ] ],
    [ "exception", null, [
      [ "frechet::app::InterruptedException", "classfrechet_1_1app_1_1InterruptedException.html", null ]
    ] ],
    [ "frechet::app::FileEntry", "structfrechet_1_1app_1_1FileEntry.html", null ],
    [ "frechet::poly::DoubleEndedQueue< T >::Frame", "structfrechet_1_1poly_1_1DoubleEndedQueue_1_1Frame.html", null ],
    [ "frechet::fs::FreeSpace", "classfrechet_1_1fs_1_1FreeSpace.html", null ],
    [ "frechet::reach::FSPath", "classfrechet_1_1reach_1_1FSPath.html", [
      [ "frechet::poly::PolygonFSPath", "classfrechet_1_1poly_1_1PolygonFSPath.html", null ]
    ] ],
    [ "frechet::reach::Graph", "classfrechet_1_1reach_1_1Graph.html", [
      [ "frechet::reach::GraphCL", "classfrechet_1_1reach_1_1GraphCL.html", null ]
    ] ],
    [ "frechet::reach::GraphModel", "classfrechet_1_1reach_1_1GraphModel.html", null ],
    [ "frechet::reach::GraphModelAxis", "classfrechet_1_1reach_1_1GraphModelAxis.html", null ],
    [ "frechet::k::kAlgorithm::Greedy", "structfrechet_1_1k_1_1kAlgorithm_1_1Greedy.html", null ],
    [ "frechet::fs::Grid", "classfrechet_1_1fs_1_1Grid.html", null ],
    [ "frechet::reach::IndexRange", "structfrechet_1_1reach_1_1IndexRange.html", null ],
    [ "frechet::input::InputReader", "classfrechet_1_1input_1_1InputReader.html", null ],
    [ "frechet::data::Interval", "classfrechet_1_1data_1_1Interval.html", [
      [ "frechet::k::MappedInterval", "classfrechet_1_1k_1_1MappedInterval.html", null ],
      [ "frechet::reach::BoundarySegment", "classfrechet_1_1reach_1_1BoundarySegment.html", null ]
    ] ],
    [ "frechet::data::IntervalPair", "classfrechet_1_1data_1_1IntervalPair.html", null ],
    [ "frechet::data::Array2D< T >::iterator", "classfrechet_1_1data_1_1Array2D_1_1iterator.html", null ],
    [ "frechet::data::BitSet::iterator", "classfrechet_1_1data_1_1BitSet_1_1iterator.html", null ],
    [ "Kernel", null, [
      [ "frechet::poly::PolygonTraits", "structfrechet_1_1poly_1_1PolygonTraits.html", null ]
    ] ],
    [ "frechet::data::MatrixPool", "classfrechet_1_1data_1_1MatrixPool.html", null ],
    [ "frechet::reach::Graph::Origin", "structfrechet_1_1reach_1_1Graph_1_1Origin.html", null ],
    [ "pair", null, [
      [ "frechet::poly::Segment", "classfrechet_1_1poly_1_1Segment.html", null ]
    ] ],
    [ "frechet::view::Palette", "classfrechet_1_1view_1_1Palette.html", null ],
    [ "Partition_traits_2", null, [
      [ "frechet::poly::PartitionTraits", "structfrechet_1_1poly_1_1PartitionTraits.html", null ]
    ] ],
    [ "frechet::reach::Placement", "structfrechet_1_1reach_1_1Placement.html", null ],
    [ "Point", null, [
      [ "frechet::poly::Vertex_base< TDS >", "classfrechet_1_1poly_1_1Vertex__base.html", null ]
    ] ],
    [ "Point_2", null, [
      [ "frechet::poly::CgalPoint", "classfrechet_1_1poly_1_1CgalPoint.html", null ]
    ] ],
    [ "frechet::reach::PointerInterval", "structfrechet_1_1reach_1_1PointerInterval.html", [
      [ "frechet::reach::BoundarySegment", "classfrechet_1_1reach_1_1BoundarySegment.html", null ]
    ] ],
    [ "frechet::poly::PolygonShortestPaths", "classfrechet_1_1poly_1_1PolygonShortestPaths.html", [
      [ "frechet::poly::PolygonShortestPathsFS", "classfrechet_1_1poly_1_1PolygonShortestPathsFS.html", null ]
    ] ],
    [ "frechet::poly::PolygonUtilities", "classfrechet_1_1poly_1_1PolygonUtilities.html", null ],
    [ "QAbstractMessageHandler", null, [
      [ "frechet::input::InputReader::XmlMessageHandler", "classfrechet_1_1input_1_1InputReader_1_1XmlMessageHandler.html", null ]
    ] ],
    [ "QFrame", null, [
      [ "frechet::view::BaseView", "classfrechet_1_1view_1_1BaseView.html", [
        [ "frechet::view::CurveView", "classfrechet_1_1view_1_1CurveView.html", null ],
        [ "frechet::view::FreeSpaceView", "classfrechet_1_1view_1_1FreeSpaceView.html", null ]
      ] ]
    ] ],
    [ "QGraphicsItemGroup", null, [
      [ "frechet::view::CellView", "classfrechet_1_1view_1_1CellView.html", null ],
      [ "frechet::view::IntervalView", "classfrechet_1_1view_1_1IntervalView.html", null ]
    ] ],
    [ "QGraphicsLineItem", null, [
      [ "frechet::view::GraphicsHoverLineItem", "classfrechet_1_1view_1_1GraphicsHoverLineItem.html", null ]
    ] ],
    [ "QGraphicsView", null, [
      [ "frechet::view::GraphicsView", "classfrechet_1_1view_1_1GraphicsView.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "frechet::view::MainWindow", "classfrechet_1_1view_1_1MainWindow.html", null ]
    ] ],
    [ "QObject", null, [
      [ "frechet::app::FileHistory", "classfrechet_1_1app_1_1FileHistory.html", null ],
      [ "frechet::app::FrechetViewApplication", "classfrechet_1_1app_1_1FrechetViewApplication.html", null ],
      [ "frechet::k::kAlgorithm", "classfrechet_1_1k_1_1kAlgorithm.html", null ],
      [ "frechet::poly::Algorithm", "classfrechet_1_1poly_1_1Algorithm.html", [
        [ "frechet::poly::AlgorithmSingleCore", "classfrechet_1_1poly_1_1AlgorithmSingleCore.html", [
          [ "frechet::poly::AlgorithmMultiCore", "classfrechet_1_1poly_1_1AlgorithmMultiCore.html", [
            [ "frechet::poly::AlgorithmTopoSort", "classfrechet_1_1poly_1_1AlgorithmTopoSort.html", null ]
          ] ]
        ] ]
      ] ],
      [ "Path", "classPath.html", null ]
    ] ],
    [ "QRunnable", null, [
      [ "frechet::app::WorkerJob", "classfrechet_1_1app_1_1WorkerJob.html", [
        [ "frechet::k::KWorkerJob", "classfrechet_1_1k_1_1KWorkerJob.html", null ],
        [ "frechet::poly::PolygonWorkerJob", "classfrechet_1_1poly_1_1PolygonWorkerJob.html", [
          [ "frechet::poly::DecideWorkerJob", "classfrechet_1_1poly_1_1DecideWorkerJob.html", null ],
          [ "frechet::poly::OptimiseCurveWorkerJob", "classfrechet_1_1poly_1_1OptimiseCurveWorkerJob.html", null ],
          [ "frechet::poly::OptimisePolyWorkerJob", "classfrechet_1_1poly_1_1OptimisePolyWorkerJob.html", null ]
        ] ]
      ] ]
    ] ],
    [ "QWidget", null, [
      [ "frechet::view::ControlPanel", "classfrechet_1_1view_1_1ControlPanel.html", null ]
    ] ],
    [ "frechet::poly::Vertex_base< TDS >::Rebind_TDS< TDS2 >", "structfrechet_1_1poly_1_1Vertex__base_1_1Rebind__TDS.html", null ],
    [ "frechet::data::Rect", "structfrechet_1_1data_1_1Rect.html", null ],
    [ "frechet::reach::Structure::SingleCellAuxData", "structfrechet_1_1reach_1_1Structure_1_1SingleCellAuxData.html", null ],
    [ "frechet::data::Spirolator", "classfrechet_1_1data_1_1Spirolator.html", null ],
    [ "frechet::reach::Structure", "classfrechet_1_1reach_1_1Structure.html", null ],
    [ "frechet::reach::StructureIterator", "classfrechet_1_1reach_1_1StructureIterator.html", null ],
    [ "frechet::reach::StructureTask", "classfrechet_1_1reach_1_1StructureTask.html", [
      [ "frechet::reach::CalculateTask", "classfrechet_1_1reach_1_1CalculateTask.html", null ],
      [ "frechet::reach::MergeTask", "classfrechet_1_1reach_1_1MergeTask.html", null ]
    ] ],
    [ "frechet::poly::Triangulation", "classfrechet_1_1poly_1_1Triangulation.html", null ],
    [ "Triangulation_ds_vertex_base_2", null, [
      [ "frechet::poly::Vertex_base< TDS >", "classfrechet_1_1poly_1_1Vertex__base.html", null ]
    ] ],
    [ "vector", null, [
      [ "frechet::fs::GridAxis", "classfrechet_1_1fs_1_1GridAxis.html", null ]
    ] ],
    [ "frechet::app::WorkerJobHandle", "classfrechet_1_1app_1_1WorkerJobHandle.html", null ],
    [ "frechet::k::kAlgorithm::WorkingSet", "structfrechet_1_1k_1_1kAlgorithm_1_1WorkingSet.html", null ]
];