var classfrechet_1_1app_1_1FileHistory =
[
    [ "FileHistory", "classfrechet_1_1app_1_1FileHistory.html#a841820a546629b57a1c779b6d1fd5b01", null ],
    [ "attachMenu", "classfrechet_1_1app_1_1FileHistory.html#a201ef4fe375cb020474a56b38c429331", null ],
    [ "beginFile", "classfrechet_1_1app_1_1FileHistory.html#acc2fe8db717066fffe8e433b59f72c7c", null ],
    [ "createMenuItem", "classfrechet_1_1app_1_1FileHistory.html#a0e416212829db76181424ba93ff3a9d7", null ],
    [ "endFile", "classfrechet_1_1app_1_1FileHistory.html#ada6babf87e1b002cac11668b29b1ec72", null ],
    [ "indexOf", "classfrechet_1_1app_1_1FileHistory.html#a2831902863f664a55d9a673e494db576", null ],
    [ "indexOf", "classfrechet_1_1app_1_1FileHistory.html#a3c2b8232d487b7ebaedc2c4f04eb289c", null ],
    [ "init", "classfrechet_1_1app_1_1FileHistory.html#a256c96cd23ac579f1752cd6fb96cb898", null ],
    [ "insert", "classfrechet_1_1app_1_1FileHistory.html#a06470f373ffae3a0c090106cc0b05d99", null ],
    [ "lastFile", "classfrechet_1_1app_1_1FileHistory.html#a7df8a60f0d0920392dd7dcfa74cc7521", null ],
    [ "lastId", "classfrechet_1_1app_1_1FileHistory.html#a9922e07159025b585cd257b82bb4f66e", null ],
    [ "moveMenuItem", "classfrechet_1_1app_1_1FileHistory.html#abbff89d5b6eee694cc4026ba66876660", null ],
    [ "onMenuSelected", "classfrechet_1_1app_1_1FileHistory.html#a31ee97eb3eb5ffe73ed2a4f3c4c972f9", null ],
    [ "open", "classfrechet_1_1app_1_1FileHistory.html#a8cdb67d4f82e0747b1a5ab951c182fca", null ],
    [ "restore", "classfrechet_1_1app_1_1FileHistory.html#a8d9e60d881348d36b4516c42f17148f6", null ],
    [ "files", "classfrechet_1_1app_1_1FileHistory.html#ac102d7cc6e76af1b1714296e90f12a3b", null ],
    [ "groupName", "classfrechet_1_1app_1_1FileHistory.html#a00d6358fc33351fe05709d36bd51f828", null ],
    [ "menu", "classfrechet_1_1app_1_1FileHistory.html#ad78c3d00b590d38939c6e46bb3986374", null ],
    [ "next_id", "classfrechet_1_1app_1_1FileHistory.html#aabf58fe35dc9561c0e13d04d115ca665", null ]
];